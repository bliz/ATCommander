﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using LittleUmph;
using ATCommander.DataTypes.Validators;
using ATCommander.DataFields.Formatters;

namespace ATCommander.DataTypes
{
    [Serializable]
    public partial class DataField : UserControl
    {
        protected const int INT_MarginOffset = 10;

        #region [ Auto Gen Stuff ]
        protected FlowLayoutPanel flwInputContainer;
        protected Label lblFieldName;
        private ToolTip toolTip;
        private IContainer components;
        protected Label lblErrorMessage; 
        #endregion
        
        #region [ Properties ]
        public ATArg Argument { get; set; }
        public List<AValidator> Validators { get; set; }

        private string _HelpText;
        public string HelpText
        {
            get
            {
                return _HelpText;
            }
            set
            {
                _HelpText = value;

                if (value != null && Str.IsNotEmpty(value) && InputControl != null)
                {
                    toolTip.SetToolTip(InputControl, value);
                }
            }
        }

        public Control InputControl { get; set; }
        public AFormatter Formatter { get; set; }

        /// <summary>
        /// The origin string from the variable string (use this to replace value with the template)
        /// </summary>
        /// <value>
        /// The replacement string.
        /// </value>
        public string ReplacementString { get; set; }
        #endregion

        #region [ Constructors ]
        public DataField()
            : this("DataField", new RawFormat())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DataField"/> class.
        /// </summary>
        /// <param name="fieldName">Name of the field.</param>
        public DataField(string fieldName, AFormatter formatter)
        {
            Formatter = formatter;

            Argument = new ATArg();
            Validators = new List<AValidator>();

            InitializeComponent();

            lblFieldName.Text = Str.ToTitleCase(fieldName) + ":";
            lblErrorMessage.Text = "";
        }
        #endregion

        #region [ Virtual Functions ]
        /// <summary>
        /// Get the current value of the data field (formated in the correct syntax, ie include quote if needed)
        /// </summary>
        /// <returns></returns>
        public virtual string GetValue()
        {
            return Formatter.FormatValue(InputControl.Text);
        }


        /// <summary>
        /// Determines whether this instance is valid.
        /// </summary>
        /// <returns></returns>
        public virtual bool IsValid()
        {
            var value = InputControl.Text;
            if (Argument.Required && Str.IsEmpty(value))
            {
                lblErrorMessage.Text = "This sucker cannot be empty";
                return false;
            }

            foreach (var val in Validators)
            {
                if (!val.IsValid(value))
                {
                    lblErrorMessage.Text = val.ErrorMessage;
                    return false;
                }
            }
            lblErrorMessage.Text = "";
            return true;
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public virtual void Clear()
        {
            if (InputControl is TextBox)
            {
                InputControl.Text = "";
            }
            else if (InputControl is ComboBox)
            {
                ComboBox combo = (ComboBox)InputControl;
                if (combo.Items.Count == 0)
                {
                    combo.SelectedIndex = -1;
                }
                else
                {
                    combo.SelectedIndex = 0;
                }
            }
        }
        #endregion

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flwInputContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.lblFieldName = new System.Windows.Forms.Label();
            this.lblErrorMessage = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // flwInputContainer
            // 
            this.flwInputContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flwInputContainer.Location = new System.Drawing.Point(0, 20);
            this.flwInputContainer.Name = "flwInputContainer";
            this.flwInputContainer.Size = new System.Drawing.Size(254, 37);
            this.flwInputContainer.TabIndex = 0;
            // 
            // lblFieldName
            // 
            this.lblFieldName.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblFieldName.Location = new System.Drawing.Point(0, 0);
            this.lblFieldName.Name = "lblFieldName";
            this.lblFieldName.Size = new System.Drawing.Size(254, 21);
            this.lblFieldName.TabIndex = 1;
            this.lblFieldName.Text = "Field Label:";
            // 
            // lblErrorMessage
            // 
            this.lblErrorMessage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblErrorMessage.Font = new System.Drawing.Font("Consolas", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorMessage.ForeColor = System.Drawing.Color.Crimson;
            this.lblErrorMessage.Location = new System.Drawing.Point(0, 59);
            this.lblErrorMessage.Name = "lblErrorMessage";
            this.lblErrorMessage.Size = new System.Drawing.Size(254, 20);
            this.lblErrorMessage.TabIndex = 2;
            this.lblErrorMessage.Text = "Error Message";
            this.lblErrorMessage.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DataField
            // 
            this.Controls.Add(this.flwInputContainer);
            this.Controls.Add(this.lblErrorMessage);
            this.Controls.Add(this.lblFieldName);
            this.Font = new System.Drawing.Font("Consolas", 14.25F);
            this.Name = "DataField";
            this.Size = new System.Drawing.Size(254, 79);
            this.ResumeLayout(false);

        }
    }
}
