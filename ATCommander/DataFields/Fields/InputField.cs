﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using LittleUmph;
using System.Windows.Forms;
using ATCommander.DataFields.Formatters;

namespace ATCommander.DataTypes
{
    public class InputField: DataField
    {
        public InputField(string fieldName, AFormatter formatter) : base(fieldName, formatter)
        {
            var txt = new TextBox();
            txt.Width = flwInputContainer.Width - DataField.INT_MarginOffset;
            txt.Name = Str.CleanVarName(fieldName);

            base.InputControl = txt;
            base.flwInputContainer.Controls.Add(base.InputControl);
        }
    }
}
