﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using LittleUmph;

namespace ATCommander
{
    public partial class FrmATSelection : Form
    {
        public FileInfo ATFile
        {
            get
            {
                if (ltAtList.SelectedIndex != -1)
                {
                    var selectedFile = ltAtList.SelectedItem.ToString();
                    var atFile = new FileInfo(Path.Combine(Properties.Settings.Default.AtCommandPath, selectedFile));
                    if (atFile.Exists)
                    {
                        return atFile;
                    }
                }
                return null;
            }
        }

        public FrmATSelection()
        {
            InitializeComponent();
        }

        private void FrmATSelection_Load(object sender, EventArgs e)
        {
            DirectoryInfo root = new DirectoryInfo(Properties.Settings.Default.AtCommandPath);

            foreach (var f in root.GetFiles("*.json"))
            {
                ltAtList.Items.Add(f.Name);
            }

        }

        private void ltAtList_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSelect.Enabled = ltAtList.SelectedIndex != -1;            
        }

        private void ltAtList_DoubleClick(object sender, EventArgs e)
        {
            if (ATFile != null)
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}
