﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

using ATCommander.DataTypes;
using LittleUmph;
using LittleUmph.GUI.Components;

namespace ATCommander
{
    public partial class FrmInput : Form
    {
        string _groupName;
        DataStore _storage;

        public ATCommand ATCommand { get; set; }

        #region [ Constructors ]
        public FrmInput(SettingSaver settingSaver, ATCommand atCommand)
        {
            string cmdText = atCommand.Command;
            _groupName = Str.CleanVarName(cmdText);
            _storage = settingSaver.DataStore;

            InitializeComponent();

            lblTemplate.Text = cmdText;
            ATCommand = atCommand;

            var fields = atCommand.GetDataFields();
            if (fields == null || fields.Count == 0)
            {
                return;
            }

            foreach (var f in fields)
            {
                f.MouseHover += SetToolTip;
                flwFields.Controls.Add(f);
            }

            if (_storage.GroupExist(_groupName))
            {
                var controls = GetInputControls();
                _storage.LoadCollection(_groupName, controls);
            }
        }

        private Control[] GetInputControls()
        {
            var dataFields = flwFields.Controls.OfType<DataField>();
            var controls = dataFields.Select(df => df.InputControl).ToArray();
            return controls;
        }
        #endregion

        #region [ Return the Result ]
        public string GetResult()
        {
            string result = ATCommand.Command;
            foreach (DataField c in flwFields.Controls)
            {
                if (c.IsValid())
                {
                    result = result.Replace(c.ReplacementString, c.GetValue());
                }
            }
            return result;
        }
        #endregion

        #region [ Ok Press / Save Input ]
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (chkRemember.Checked)
            {
                _storage.SaveCollection(_groupName, GetInputControls());
                _storage.SaveToFile();
            }
            DialogResult = DialogResult.OK;
        }
        #endregion

        #region [ Helper ]
        private void FrmInput_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (DialogResult == DialogResult.OK)
            {
                foreach (DataField c in flwFields.Controls)
                {
                    if (!c.IsValid())
                    {
                        e.Cancel = true;
                    }
                }
            }
        }

        private void SetToolTip(object sender, EventArgs e)
        {
            var c = (Control)sender;
            toolTip.SetToolTip(c, c.Text);
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            foreach (var c in flwFields.Controls)
            {
                ((DataField)c).Clear();
            }
        }
        #endregion
    }
}
