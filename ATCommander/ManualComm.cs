﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using LittleUmph;

namespace ATCommander
{
    public partial class ManualComm : UserControl
    {
        public delegate void SendDataHandler(ManualComm sender, string data);
        /// <summary>
        /// 
        /// </summary>
        [Category("[ ManualComm ]")]
        public event SendDataHandler SendData;

        public override string Text
        {
            get
            {
                return txtInput.Text;
            }
            set
            {
                txtInput.Text = value;
            }
        }

        public ManualComm()
        {
            InitializeComponent();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            string data = txtInput.Text;
            if (chkHex.Checked)
            {
                data = Hex.ToString(data);
            }

            Dlgt.Invoke(SendData, this, data);
        }
    }
}
